package pdfcpu

import (
	"bitbucket.org/silviolucas/pdfcpu/pkg/filter"
	"bitbucket.org/silviolucas/pdfcpu/pkg/log"
	"github.com/pkg/errors"
)

// ExtractAllFontData extract all embedded fonts
// returns the font object and the stream dictionary
func ExtractAllFontData(ctx *Context, objNr int) (*FontObject, *StreamDict, error) {

	fontObject := ctx.Optimize.FontObjects[objNr]

	// Only embedded fonts have binary data.
	if !fontObject.Embedded() {
		log.Debug.Printf("extractFontData: ignoring obj#%d - non embedded font: %s\n", objNr, fontObject.FontName)
		return nil, nil, nil
	}

	d, err := fontDescriptor(ctx.XRefTable, fontObject.FontDict, objNr)
	if err != nil {
		return nil, nil, err
	}

	if d == nil {
		log.Debug.Printf("extractFontData: ignoring obj#%d - no fontDescriptor available for font: %s\n", objNr, fontObject.FontName)
		return nil, nil, nil
	}

	ir := fontDescriptorFontFileIndirectObjectRef(d)
	if ir == nil {
		log.Debug.Printf("extractFontData: ignoring obj#%d - no font file available for font: %s\n", objNr, fontObject.FontName)
		return nil, nil, nil
	}

	sd, err := ctx.DereferenceStreamDict(*ir)
	if err != nil {
		return nil, nil, err
	}
	if sd == nil {
		return nil, nil, errors.Errorf("extractFontData: corrupt font obj#%d for font: %s\n", objNr, fontObject.FontName)
	}

	// Decode streamDict if used filter is supported only.
	err = decodeStream(sd)
	if err == filter.ErrUnsupportedFilter {
		return nil, nil, nil
	}
	if err != nil {
		return nil, nil, err
	}

	fontObject.Data = sd.Content

	fontType := fontObject.SubType()
	switch fontType {

	case "TrueType":

		fontObject.Extension = "ttf"

	default:
		fontObject.Extension = fontType
	}

	return fontObject, sd, nil
}

func ExtractFontFromDict(ctx *Context, dict Dict, objNr int) (*StreamDict, error) {
	d, err := fontDescriptor(ctx.XRefTable, dict, objNr)
	if err != nil {
		return nil, err
	}

	if d == nil {
		log.Debug.Printf("extractFontData: ignoring obj#%d - no fontDescriptor available\n", objNr)
		return nil, nil
	}

	ir := fontDescriptorFontFileIndirectObjectRef(d)
	if ir == nil {
		log.Debug.Printf("extractFontData: ignoring obj#%d - no font file available\n", objNr)
		return nil, nil
	}

	sd, err := ctx.DereferenceStreamDict(*ir)
	if err != nil {
		return nil, err
	}
	if sd == nil {
		return nil, errors.Errorf("extractFontData: corrupt font obj#%d\n", objNr)
	}

	return sd, nil

}
