package pdfcpu

import (
	"bitbucket.org/silviolucas/pdfcpu/pkg/log"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"testing"
)

func TestSplitDocumentReadAll(t *testing.T) {

	log.SetDefaultInfoLogger()

	data, e := SplitDocument("/Users/silvio/temp/agencia-pro/b.pdf")

	defer data.Close()

	if e != nil {
		panic(e.Error())
	}

	//	b := make([]byte, data.BufferLen())

	b, e := data.ReadAll()

	if e != nil {
		t.Error(e.Error())
	}

	//if i != len(b) {
	//	t.Error("Invalid read")
	//}
	//
	// save part of PDF
	file, e := os.Create("../../pdfs/part1")

	if e != nil {
		t.Error(e.Error())
	}

	size := len(b)
	file.Write(b)

	for i := 0; i < len(data.data); i++ {
		s := strconv.FormatInt(int64(i), 10)
		fileAsset, e := os.Create("../../pdfs/asset" + s)
		if e != nil {
			t.Error(e.Error())
		}
		fileAsset.Write(data.data[i].Raw)
		fileAsset.Close()
		size += len(data.data[i].Raw)
	}

	pdfOrig, _ := os.Open("/Users/silvio/temp/agencia-pro/b.pdf")
	s, _ := pdfOrig.Stat()

	if int64(size) != s.Size() {
		t.Error("invalid size")
	}

	pdfOrig.Close()

	// mount the pdf again
	// read part1
	file.Seek(0, 0)
	file.Read(b)
	current := int64(0)
	lastOffset := int64(0)
	lastSize := int64(0)
	total := 0

	w, err := os.Create("../../pdfs/a.pdf")

	if err != nil {
		t.Error(err)
	}

	for i := 0; i < len(data.data); i++ {
		d := data.GetData(i)
		max := d.StreamOffset - (lastSize + lastOffset) + current
		n, _ := w.Write(b[current:max])
		total += n
		current = max
		lastOffset = d.StreamOffset
		lastSize = int64(*d.StreamLength)
		n, _ = w.Write(d.Raw)
		total += n
	}

	missing := size - total
	if missing != 0 {
		missing += int(current)
		n, _ := w.Write(b[current:missing])
		total += n
	}
}

const BUFFER_SIZE = 4 * 4096

func TestSplitDocumentRead(t *testing.T) {

	log.SetDefaultInfoLogger()

	data, e := SplitDocument("../../pdfs/LIVE_0099334292_201811_3568682800.pdf")

	defer data.Close()

	if e != nil {
		t.Error(e.Error())
	}

	size := 0
	// save part of PDF
	file, e := os.Create("../../pdfs/part1")
	for {
		b := make([]byte, BUFFER_SIZE)

		i, e := data.Read(b)

		if e != nil {
			if e == io.EOF {
				break
			}
			t.Error(e.Error())
		}

		size += i
		file.Write(b)
	}

	file.Close()

	for i := 0; i < len(data.data); i++ {
		s := strconv.FormatInt(int64(i), 10)
		fileAsset, e := os.Create("../../pdfs/asset" + s)
		if e != nil {
			t.Error(e.Error())
		}
		fileAsset.Write(data.data[i].Raw)
		fileAsset.Close()
		size += len(data.data[i].Raw)
	}

	pdfOrig, _ := os.Open("../../pdfs/LIVE_0099334292_201811_3568682800.pdf")
	s, _ := pdfOrig.Stat()

	if int64(size) != s.Size() {
		t.Error("invalid size")
	}

	pdfOrig.Close()

	// mount the pdf again
	// read part1
	file, err := os.Open("../../pdfs/part1")

	if err != nil {
		t.Error(err)
	}

	b, err := ioutil.ReadAll(file)

	current := int64(0)
	lastOffset := int64(0)
	lastSize := int64(0)
	total := 0

	w, err := os.Create("../../pdfs/a.pdf")

	if err != nil {
		t.Error(err)
	}

	for i := 0; i < len(data.data); i++ {
		d := data.GetData(i)
		max := d.StreamOffset - (lastSize + lastOffset) + current
		n, _ := w.Write(b[current:max])
		total += n
		current = max
		lastOffset = d.StreamOffset
		lastSize = int64(*d.StreamLength)
		n, _ = w.Write(d.Raw)
		total += n
	}

	missing := size - total
	if missing != 0 {
		missing += int(current)
		n, _ := w.Write(b[current:missing])
		total += n
	}
}
