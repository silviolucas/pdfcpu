package pdfcpu

import (
	"bitbucket.org/silviolucas/pdfcpu/pkg/log"
	"github.com/pkg/errors"
	"io"
	"os"
	"sort"
)

// SplitData represents the PDF context and two information that can be stripped from the
type SplitData struct {
	fileIn       string
	ctx          *Context
	data         []*StreamDict
	splitLength  int64
	inMemory     bool
	bufferPos    int64
	filePos      int64
	seekReader   io.ReadSeeker
	currentChunk int
}

// BufferLen returns the length of the buffer to read all file without the assets
func (s *SplitData) BufferLen() (n int64) {
	return s.ctx.Read.FileSize - s.splitLength
}

func (s *SplitData) DataLen() int {
	return len(s.data)
}

func (s *SplitData) GetData(i int) *StreamDict {
	return s.data[i]
}

func (s *SplitData) GetRawData(i int) ([]byte, error) {
	sd := s.data[i]
	raw := sd.Raw
	if raw != nil {
		return raw, nil
	}
	if _, err := s.seekReader.Seek(sd.StreamOffset, io.SeekStart); err != nil {
		return nil, err
	}
	buff := make([]byte, *sd.StreamLength)
	n, err := s.seekReader.Read(buff)
	if err != nil {
		return nil, err
	}
	if int64(n) != *sd.StreamLength {
		return nil, errors.New("invalid bytes read from " + s.fileIn)
	}
	return buff, nil
}

func (s *SplitData) Close() error {
	if !s.inMemory {
		return s.seekReader.(*os.File).Close()
	}
	return nil
}

func (s *SplitData) Reset() error {

	if !s.inMemory {
		file, err := os.Open(s.fileIn)
		if err != nil {
			return err
		}
		s.seekReader = file
		fileInfo, err := file.Stat()
		if err != nil {
			return err
		}
		s.ctx.Read.FileSize = fileInfo.Size()
	} else {
		s.ctx.Read.rs.Seek(0, 0)
		s.seekReader = s.ctx.Read.rs
	}

	s.bufferPos = int64(0)
	s.filePos = int64(0)
	s.currentChunk = 0

	return nil
}

// ReadAll read the file completely inside a buffer
//
// Deprecated: this methods will not work for very large files
func (s *SplitData) ReadAll() ([]byte, error) {
	/**
	 * offset 0
	 * --- data ---
	 * offset data[0].StreamOffset
	 * --- asset ---
	 * offset data[0].StreamOffset + data[0].StreamLength
	 * --- data ---
	 * offset data[1].StreamOffset
	 * --- asset ---
	 * offset data[1].StreamOffset + data[1].StreamLength
	 * ...
	 * --- data ---
	 * offset cts.FileSize = EOF
	 */

	var seekReader io.ReadSeeker

	if !s.inMemory {
		file, err := os.Open(s.fileIn)

		if err != nil {
			return nil, err
		}

		defer file.Close()
		seekReader = file
	} else {
		seekReader = s.ctx.Read.rs
	}

	p := make([]byte, s.BufferLen())

	bufferLen := int64(len(p))
	if bufferLen < s.BufferLen() {
		return nil, errors.New("invalid buffer size")
	}
	bufferPos := int64(0)
	filePos := int64(0)
	for currentChunk := 0; len(s.data) > currentChunk; currentChunk++ {
		lengthReader := s.data[currentChunk].StreamOffset - filePos
		log.Info.Println("Reading range => ", filePos, s.data[currentChunk].StreamOffset)
		r := io.LimitReader(seekReader, lengthReader)
		i, err := r.Read(p[bufferPos:])
		if err != nil {
			return nil, err
		}
		bufferPos += int64(i)
		seekPos := s.data[currentChunk].StreamOffset + *s.data[currentChunk].StreamLength
		_, err = seekReader.Seek(seekPos, io.SeekStart)
		if err != nil {
			return nil, err
		}
		filePos = seekPos
	}
	if bufferPos < bufferLen {
		log.Info.Println("Reading last range => ", filePos, s.ctx.Read.FileSize)
		i, err := seekReader.Read(p[bufferPos:])
		if err != nil {
			return nil, err
		}
		bufferPos += int64(i)
		filePos += int64(i)
	}

	if bufferPos != bufferLen {
		return nil, errors.New("Invalid buffer pointer at the end")
	}

	if filePos != s.ctx.Read.FileSize {
		return nil, errors.New("Invalid file pointer at the end")
	}

	return p, nil
}

// Read the pdf without the assets bytes directly in the buffer passed as argument
func (s *SplitData) Read(p []byte) (int, error) {

	/**
	 * offset 0
	 * --- data ---
	 * offset data[0].StreamOffset
	 * --- asset ---
	 * offset data[0].StreamOffset + data[0].StreamLength
	 * --- data ---
	 * offset data[1].StreamOffset
	 * --- asset ---
	 * offset data[1].StreamOffset + data[1].StreamLength
	 * ...
	 * --- data ---
	 * offset cts.FileSize = EOF
	 */

	bufferLen := len(p)
	bufferPos := 0
	oneRead := false
	for currentChunk := s.currentChunk; len(s.data) > currentChunk; currentChunk++ {
		tempBufferLength := int(s.data[currentChunk].StreamOffset - s.filePos)
		if tempBufferLength > bufferLen {
			tempBufferLength = bufferLen
			oneRead = true
		}
		log.Info.Println("Reading range => ", s.filePos, s.data[currentChunk].StreamOffset)
		r := io.LimitReader(s.seekReader, int64(tempBufferLength))
		i, err := r.Read(p[bufferPos:])
		bufferLen -= i
		bufferPos += i
		if err != nil {
			return bufferPos, err
		}
		s.filePos += int64(i)
		if oneRead {
			s.currentChunk = currentChunk
			return bufferPos, nil
		}
		seekPos := s.data[currentChunk].StreamOffset + *s.data[currentChunk].StreamLength
		_, err = s.seekReader.Seek(seekPos, io.SeekStart)
		if err != nil {
			return bufferPos, err
		}
		s.filePos = seekPos
	}
	// we reach the end of chunks
	s.currentChunk = len(s.data)
	if bufferPos < len(p) {
		log.Info.Println("Reading last range => ", s.filePos, s.ctx.Read.FileSize)
		i, err := s.seekReader.Read(p[bufferPos:])
		if err != nil {
			return i, err
		}
		bufferPos += i
		s.filePos += int64(i)
	}

	return bufferPos, nil
}

func NewConfiguration() *Configuration {
	config := NewDefaultConfiguration()
	config.SupportDereferenceError = true
	return config
}

// SplitDocument extracts fonts and images from one PDF file
func SplitDocument(fileIn string) (*SplitData, error) {

	log.Info.Println("Processing file =>", fileIn)
	ctx, err := readAndOptimizePdf(fileIn, NewConfiguration())
	if err != nil {
		return nil, err
	}

	// select all pages
	m := IntSet{}
	for i := 1; i <= ctx.PageCount; i++ {
		m[i] = true
	}

	pdfStruct := SplitData{fileIn: fileIn, ctx: ctx}

	log.Info.Println("Extracting images for file", fileIn)

	pdfStruct.splitLength = 0

	for _, io := range ctx.Optimize.ImageObjects {

		if io != nil {
			pdfStruct.data = append(pdfStruct.data, io.ImageDict)
			pdfStruct.splitLength += *io.ImageDict.StreamLength
		}
	}

	log.Info.Println("Extracting fonts for file", fileIn)
	for objNr, _ := range ctx.Optimize.FontObjects {

		fo, sd, err := ExtractAllFontData(ctx, objNr)
		if err != nil {
			return nil, err
		}

		if fo != nil {
			pdfStruct.data = append(pdfStruct.data, sd)
			pdfStruct.splitLength += *sd.StreamLength
		}

	}

	sort.Slice(pdfStruct.data, func(i, j int) bool {
		return pdfStruct.data[i].StreamOffset < pdfStruct.data[j].StreamOffset
	})

	pdfStruct.inMemory = false
	pdfStruct.Reset()
	return &pdfStruct, nil
}

// SplitDocumentStreaming extracts fonts and images from one PDF using streaming of bytes
func SplitDocumentStreaming(rs io.ReadSeeker, size int64) (*SplitData, error) {

	log.Info.Println("Processing streaming PDF")
	ctx, err := readAndOptimizePdfStreaming(rs, NewConfiguration())
	if err != nil {
		return nil, err
	}

	// select all pages
	m := IntSet{}
	for i := 1; i <= ctx.PageCount; i++ {
		m[i] = true
	}

	pdfStruct := SplitData{ctx: ctx}

	log.Info.Println("Extracting images for streaming")

	pdfStruct.splitLength = 0

	for _, io := range ctx.Optimize.ImageObjects {

		if io != nil {
			pdfStruct.data = append(pdfStruct.data, io.ImageDict)
			pdfStruct.splitLength += *io.ImageDict.StreamLength
		}
	}

	log.Info.Println("Extracting fonts for streaming")
	for objNr, _ := range ctx.Optimize.FontObjects {

		fo, sd, err := ExtractAllFontData(ctx, objNr)
		if err != nil {
			return nil, err
		}

		if fo != nil {
			pdfStruct.data = append(pdfStruct.data, sd)
			pdfStruct.splitLength += *sd.StreamLength
		}

	}

	sort.Slice(pdfStruct.data, func(i, j int) bool {
		return pdfStruct.data[i].StreamOffset < pdfStruct.data[j].StreamOffset
	})

	pdfStruct.inMemory = true
	pdfStruct.ctx.Read.FileSize = size
	pdfStruct.Reset()
	return &pdfStruct, nil
}

func readPdf(fileIn string, config *Configuration) (ctx *Context, err error) {

	log.Info.Println("Reading pdf context for", fileIn)
	ctx, err = ReadFile(fileIn, config)
	if err != nil {
		return nil, err
	}
	//log.Info.Println("Validating XRefTable for %s", fileIn)
	//err = validate.XRefTable(ctx.XRefTable)
	//if err != nil {
	//	return nil, err
	//}

	return ctx, nil
}

func readAndOptimizePdf(fileIn string, config *Configuration) (ctx *Context, err error) {

	ctx, err = readPdf(fileIn, config)
	if err != nil {
		return nil, err
	}

	log.Info.Println("Optimizing XRefTable for", fileIn)
	err = OptimizeXRefTable(ctx)
	if err != nil {
		return nil, err
	}
	return ctx, nil
}

func readPdfStreaming(rs io.ReadSeeker, config *Configuration) (ctx *Context, err error) {

	log.Info.Println("Reading pdf context for streaming")
	ctx, err = Read(rs, config)
	if err != nil {
		return nil, err
	}
	//log.Info.Println("Validating XRefTable for %s", fileIn)
	//err = validate.XRefTable(ctx.XRefTable)
	//if err != nil {
	//	return nil, err
	//}

	ctx.Read.rs.Seek(0, 0)
	return ctx, nil
}

func readAndOptimizePdfStreaming(rs io.ReadSeeker, config *Configuration) (ctx *Context, err error) {

	ctx, err = readPdfStreaming(rs, config)
	if err != nil {
		return nil, err
	}

	log.Info.Println("Optimizing XRefTable for streaming")
	err = OptimizeXRefTable(ctx)
	if err != nil {
		return nil, err
	}
	return ctx, nil
}
